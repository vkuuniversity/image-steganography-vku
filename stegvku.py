from PIL import Image
import argparse


def extract_data(output_path):
    image = Image.open(output_path)
    pixels = image.load()
    # Get image dimensions
    width, height = image.size
    # Iterate through each pixel
    strmess = ""
    for y in range(height):
        for x in range(width):
            r, g, b = pixels[x, y]
            strmess += str(r & 1)
            strmess += str(g & 1)
            strmess += str(b & 1)
    # secret_text=""
    # for i in range(0, len(strmess), 8):
    #     char = chr(int(strmess[i:i+8], 2))
    #     secret_text += char
    image.close()
    return str(strmess)


def hiden_data(image_path, output_path, secret_text):
    image = Image.open(image_path)
    pixels = image.load()
    width, height = image.size

    binary_secret_text = secret_text
    binary_string_len_of_file = str(bin(len(binary_secret_text)))[2:]
    padded_binary_string = binary_string_len_of_file.rjust(1000, '0')
    index = 0
    binary_secret_text = padded_binary_string + binary_secret_text

    image_capacity = image.width * image.height * 3
    if len(binary_secret_text) > image_capacity:
        raise ValueError(
            "Image does not have sufficient capacity to hide the secret text.")
    # Iterate through each pixel
    for y in range(height):
        for x in range(width):
            r, g, b = pixels[x, y]
            if index < (len(binary_secret_text)):
                r = (r & 0xFE) | int(binary_secret_text[index])
                index += 1
            if index < (len(binary_secret_text)):
                g = (g & 0xFE) | int(binary_secret_text[index])
                index += 1
            if index < (len(binary_secret_text)):
                b = (b & 0xFE) | int(binary_secret_text[index])
                index += 1

            pixels[x, y] = (r, g, b)
    image.save(output_path+".png")
    image.close()
    print("[+] Success hiden data with name: "+output_path+".png")


def file_to_binary(file_path):
    try:
        with open(file_path, 'rb') as file:
            file_content = file.read()
            binary_string = ''.join(format(byte, '08b')
                                    for byte in file_content)
            return binary_string
    except FileNotFoundError:
        print("File not found!")


def binary_to_file(binary_data, output_file_path):
    try:
        lengh_of_file = binary_data[:1000]
        integer_value = int(lengh_of_file, 2)
        binary_data = binary_data[1000:1000+integer_value]
        # print(binary_data)
        with open(output_file_path, 'wb') as file:
            byte_data = bytearray(
                int(binary_data[i:i+8], 2) for i in range(0, integer_value, 8))
            file.write(byte_data)
        print("File successfully created:", output_file_path)
    except Exception as e:
        print("Error:", e)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('type', help='(hiden Or extract)')
    parser.add_argument(
        '--secretfile', help='enter file contain secrect key. Ex: key.txt')
    parser.add_argument(
        '--inhidenImg', help='enter file image you want hiden data. Ex: origin.png')
    parser.add_argument(
        '--outhidenImg', help='enter name output. Ex: hidden_data')
    parser.add_argument(
        '--filehiden', help='enter file name data hidden. Ex: hidden_data.png')
    parser.add_argument(
        '--fileExtract', help='enter file name extracted. Ex: clean_data.txt')
    args = parser.parse_args()

    if args.type == 'hiden':
        if args.type in ['hiden'] and not args.secretfile:
            parser.error("--secretfile is required when action")
        if args.type in ['hiden'] and not args.inhidenImg:
            parser.error("--inhidenImg is required when action")
        if args.type in ['hiden'] and not args.outhidenImg:
            parser.error("--outhidenImg is required when action")

        image_path = args.inhidenImg
        secret_file = args.secretfile
        outhidenImg = args.outhidenImg
        secret_mess = file_to_binary(secret_file)
        # print(secret_mess)
        hiden_data(image_path=image_path, output_path=outhidenImg,
                   secret_text=secret_mess)
    elif args.type == 'extract':
        if args.type in ['extract'] and not args.filehiden:
            parser.error("--filehiden is required when action")
        if args.type in ['extract'] and not args.fileExtract:
            parser.error("--fileExtract is required when action")
        binary_data = extract_data(output_path=args.filehiden)
        binary_to_file(binary_data=binary_data,
                       output_file_path=args.fileExtract)
    else:
        print("NOT ACTION")
